== ﻿Lista de comida

// Ordenada por orden alfabético en cada apartado

=== Aperitivos

* Empanada de atún
* Patatas fritas
* Canapés de suflé de queso
* Almejas a la marinera

=== Platos principales

* Chuletas de cabeza
* Salmón en salsa de cilantro

=== Postres

* Arroz con leche
* Tiramisú
* Tarta de queso
